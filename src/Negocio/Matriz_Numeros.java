package Negocio;

import Modelo.ListaNumeros;

/**
 * Modelamiento de una matriz usando el concepto de vector de vectores
 *
 * @author ingrid florez - 1152123
 */
public class Matriz_Numeros {

    public ListaNumeros[] filas;

    public Matriz_Numeros() {
    }

    public Matriz_Numeros(int cantFilas) {
        if (cantFilas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
    }

    /**
     * Creación de matrices cuadradas o bien rectangulares
     *
     * @param cantFilas cantidad filas de la matriz
     * @param cantColumnas cantidad columna de la matriz
     */
    public Matriz_Numeros(int cantFilas, int cantColumnas) {
        if (cantFilas <= 0 || cantColumnas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas o columnas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
        //Creando columnas:
        for (int i = 0; i < cantFilas; i++) {
            this.filas[i] = new ListaNumeros(cantColumnas);
        }

    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice fuera de rango para una fila:" + i);
        }
    }

    public void adicionarVector(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    @Override
    public String toString() {
        String msg = "";

        for (ListaNumeros myLista : this.filas) {
            msg += myLista.toString() + "\n";
        }
        return msg;
    }

    public int length_filas() {
        return this.filas.length;
    }

    /**
     * Crea una matriz a partir de una cadena formateada: elemento1,
     * elemento2... ; elemento.....; .... Cada elemento separado por una "," y
     * cada fila por un ";"
     *
     * @param cadena un String que contiene los datos de la matriz
     */
    public Matriz_Numeros(String cadena) {
        if (cadena.isEmpty()) {
            throw new RuntimeException("No se puede cargar la matriz, cadena vacía");
        }

        /**
         * Crear los datos para la filas:
         */
        String filaDatos[] = cadena.split(";");

        //2. Crear las filas:
        this.filas = new ListaNumeros[filaDatos.length];
        for (int i = 0; i < filaDatos.length; i++) {
            String columnaDatos[] = filaDatos[i].split(",");
            ListaNumeros nuevaColumnas = new ListaNumeros(columnaDatos.length);
            this.pasarDatosColumna(nuevaColumnas, columnaDatos);
            //Ingresar la columna a la matriz:
            this.filas[i] = nuevaColumnas;

        }
    }

    private void pasarDatosColumna(ListaNumeros col, String datos[]) {

        for (int j = 0; j < datos.length; j++) {
            col.adicionar(j, Float.parseFloat(datos[j]));

        }
    }

    /**
     * Obtiene que tipo de matriz es: Cuadrada, rectangular o dispersa
     *
     * @return una cadena con el tipo de matriz
     */
    private String getTipo() {

        ListaNumeros col;
        col = this.filas[0];//se iguala al vector de la posicion 0 de la matriz
        int aux = col.length();//se le asigna el tamaño del vector columnas
        int c = 0;

        for (int i = 0; i < this.length_filas(); i++) {
            col = this.filas[i];// se iguala al vector de la posicion i de la matriz
            if (this.length_filas() == col.length()) {
                c++;
            }
            if (col.length() != aux) {
                return "Es dispersa";
            }
        }
        if (c != 0) {
            return "Cuadrada";
        } else {
            return "Rectangular";
        }
    }

    /**
     * Metodo para obtener la suma y resta entre dos matrices
     *
     * @param matriz obtiene los datos de la segunda matriz a tratar
     * @param op 0 para suma y cualquier otro numero para resta
     * @return la suma y resta de las matrices segun el parametro op
     * @throws Exception lanza una excepcion diciendo que las matrices no tienen el mismo tamaño
     *
     */
    private Matriz_Numeros getSuma_Resta(Matriz_Numeros matriz, int op) throws Exception {

        Matriz_Numeros resultado = new Matriz_Numeros(this.length_filas());

        for (int i = 0; i < this.length_filas(); i++) {
            if (this.filas[i].length() != matriz.filas[i].length() || this.length_filas() != matriz.length_filas()) {
                throw new Exception("Las matrices no tienen el mismo tamaño");
            }
            if (this.getTipo().equals(matriz.getTipo())) {
                ListaNumeros suma = new ListaNumeros(this.filas[i].length());
                ListaNumeros resta = new ListaNumeros(this.filas[i].length());
                for (int j = 0; j < this.filas[0].length(); j++) {
                    suma.adicionar(j, this.filas[i].getElemento(j) + matriz.filas[i].getElemento(j));
                    resta.adicionar(j, this.filas[i].getElemento(j) - matriz.filas[i].getElemento(j));
                }
                if (op == 0) {
                    resultado.adicionarVector(i, suma);
                } else {
                    resultado.adicionarVector(i, resta);
                }
            }
        }
        return resultado;
    }

    /**
     * Metodo que retorna la suma de dos matrices
     *
     * @param matriz obtiene los datos de la segunda matriz a tratar
     * @return Retorna la suma de la matriz
     * @throws Exception captura la excepcion del metodo getSuma_Resta
     */
    public Matriz_Numeros getSuma(Matriz_Numeros matriz) throws Exception {
        return this.getSuma_Resta(matriz, 0);
    }

    /**
     * Metodo que retorna la resta de dos matrices
     *
     * @param matriz obtiene los datos de la segunda matriz a tratar
     * @return Retorna la resta de la matriz
     * @throws Exception captura la excepcion del metodo getSuma_Resta
     */
    public Matriz_Numeros getResta(Matriz_Numeros matriz) throws Exception {
        return this.getSuma_Resta(matriz, 1);
    }

    /**
     * Metodo para obtener la multiplicacion entre dos matrices
     *
     * @param matriz obtiene los datos de la segunda matriz a tratar
     * @return la multiplicacion de las matrices
     * @throws Exception lanza la exception de que la matriz no puede ser
     * dispersa
     */
    public Matriz_Numeros getMultiplicacion(Matriz_Numeros matriz) throws Exception {
        Matriz_Numeros resultado = new Matriz_Numeros(this.length_filas());

        if ("Es dispersa".equals(this.getTipo()) || "Es dispersa".equals(matriz.getTipo()) || this.filas[0].length() != matriz.length_filas() ) {
            throw new Exception("Las matrices no se pueden multiplicar, \n cantidad de filas y columnas no validas");
        }
        for (int i = 0; i < this.length_filas(); i++) {
            ListaNumeros v = new ListaNumeros(matriz.filas[0].length());
            for (int j = 0; j < v.length(); j++) {
                float resultado1 = 0;
                for (int k = 0; k < this.filas[0].length(); k++) {
                    float multiplicacion = this.filas[i].getElemento(k) * matriz.filas[k].getElemento(j);
                    resultado1 = resultado1 + multiplicacion;
                }
                v.adicionar(j, resultado1);
            }
            resultado.adicionarVector(i, v);
        }

        return resultado;
    }

    /**
     * Metodo para obtener la transpuesta de cada matriz
     *
     * @return la transpuesta de la matriz que se esta ingresando
     * @throws Exception lanza la exception de que la matriz no puede ser
     * dispersa
     */
    public Matriz_Numeros getTranspuesta() throws Exception {
        Matriz_Numeros resultado = new Matriz_Numeros(this.filas[0].length());

        if (this.getTipo().equals("Cuadrada") || this.getTipo().equals("Rectangular")) {
            for (int i = 0; i < this.filas[0].length(); i++) {

                ListaNumeros tra = new ListaNumeros(this.length_filas());
                for (int j = 0; j < this.length_filas(); j++) {
                    tra.adicionar(j, this.filas[j].getElemento(i));
                }
                resultado.adicionarVector(i, tra);
            }

        } else {
            throw new Exception("Esta matriz no se puede transponer porque " + this.getTipo());

        }

        return resultado;
    }
}
